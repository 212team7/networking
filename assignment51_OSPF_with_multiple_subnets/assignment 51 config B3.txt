version 12.1X47-D15.4;
system {
    host-name B3;
    /* User: root Password: Rootpass */
    root-authentication {
        encrypted-password "$1$xH9xJoL6$MFOUYnZr4.Qj2NM24XInz/";
    }
}
interfaces {
    ge-0/0/3 {
        unit 0 {
            family inet {
                /* Lan3 */
                address 10.10.11.2/28;
            }
        }
    }
    ge-0/0/4 {
        unit 0 {
            family inet {
                /* Lan1 */
                address 10.10.12.2/28;
            }
        }
    }
    ge-0/0/5 {
        unit 0 {
            family inet {
                /* USERLAN1 */
                address 192.168.2.1/24;
            }
        }
    }
    lo0 {
        unit 0 {
            family inet {
                address 192.168.100.2/32;
            }
        }
    }
}
protocols {
    ospf {
        export my-route-to-USERLAN3;
        area 0.0.0.0 {
            interface lo0;
            interface ge-0/0/3.0;
            interface ge-0/0/4.0;
        }
    }
}
policy-options {
    policy-statement my-route-to-USERLAN3 {
        term accpt-my-route-to-USERLAN3 {
            from {
                route-filter 192.168.2.0/24 exact;
            }
            then accept;
        }
    }
}
security {
    policies {
        from-zone trust to-zone trust {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
    }
    zones {
        security-zone trust {
            host-inbound-traffic {
            /* Global for all interfaces */
                system-services {
                    ping;
                }
            }
            interfaces {
                ge-0/0/3.0 {
                    host-inbound-traffic {
                        protocols {
                            ospf;
                        }
                    }
                }
                lo0 {
                    host-inbound-traffic {
                        protocols {
                            ospf;
                        }
                    }
                }
                ge-0/0/4.0 {
                    host-inbound-traffic {
                        protocols {
                            ospf;
                        }
                    }
                }
                ge-0/0/5.0 {
                    host-inbound-traffic {
                        protocols {
                            ospf;
                        }
                    }
                }
            }
        }
    }
}


